package com.epam.alex.fabrics;

import javafx.scene.effect.InnerShadow;
import javafx.scene.effect.Reflection;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Box;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.transform.Rotate;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Алекс on 19.11.2016.
 */
public class NodeFabricImpl implements NodeFabric {

    /**
     * angle of initital rotate.
     */
    private static final int ANGLE = 45;
    /**
     * factor of dots count.
     */
    private static final int DOT_FACTOR = 6;

    @Override
    public final Box getCube(final int size, final Point point,
                             final Color color) {
        Box shape = new Box(size, size, size);
        shape.setTranslateX(point.getX());
        shape.setTranslateY(point.getY());
        shape.setTranslateZ(0);
        Rotate rxBox = new Rotate(ANGLE, 0, 0, 0, Rotate.X_AXIS);
        Rotate rzBox = new Rotate(ANGLE, 0, 0, 0, Rotate.Z_AXIS);
        shape.getTransforms().addAll(rxBox, rzBox);
        final PhongMaterial phongMaterial = new PhongMaterial();
        phongMaterial.setSpecularColor(Color.LIGHTSTEELBLUE);
        phongMaterial.setDiffuseColor(color);
        shape.setMaterial(phongMaterial);
        return shape;
    }


    @Override
    public final List<Circle> getCircles(final int maxWidth,
                                         final int maxHeight,
                                         final int radius) {
        List<Circle> circles = new ArrayList<>();
        Random random = new Random();
        int count = random.nextInt(maxWidth / DOT_FACTOR
                - maxWidth / (DOT_FACTOR * 2))
                + maxWidth / (DOT_FACTOR * 2);
        for (int i = 0; i < count; i++) {
            Circle circle = new Circle(radius);
            circle.setCenterX(Math.random() * maxWidth);
            circle.setCenterY(Math.random() * maxHeight);
            circles.add(circle);
            circles.add(getOffsetCircle(circle, maxWidth));
        }
        return circles;
    }


    /**
     * create offseted dot.
     *
     * @param original original dot
     * @param maxWidth width of canvas
     * @return Circle
     */
    private Circle getOffsetCircle(final Circle original, final int maxWidth) {
        Circle circle = new Circle(original.getRadius(), original.getFill());
        circle.setCenterX(original.getCenterX() + maxWidth);
        circle.setCenterY(original.getCenterY());
        return circle;
    }

    @Override
    public final Text getText(final String value, final int size,
                              final Color color) {
        Text text = new Text(value);
        text.setFont(Font.font(null, FontWeight.BOLD, size));
        text.setTextAlignment(TextAlignment.JUSTIFY);
        text.setFill(color);
        InnerShadow innerShadow = new InnerShadow();
        innerShadow.setOffsetX(2.0f);
        innerShadow.setOffsetY(2.0f);
        innerShadow.setInput(new Reflection());
        text.setEffect(innerShadow);
        return text;
    }


}
