package com.epam.alex.fabrics;

import javafx.animation.Animation;
import javafx.scene.Node;
import javafx.util.Duration;

import java.util.List;

/**
 * Created by Алекс on 19.11.2016.
 */
public interface AnimationFabric {
    /**
     * create timeline  with given params for moving node on x axis.
     * cycle count by default is equals 1.
     *
     * @param node     node for moving
     * @param duration duration of moving
     * @param startPos start position
     * @param endPos   end position
     * @return Timeline
     */
    Animation getMoving(Node node, Duration duration, int startPos, int endPos);

    /**
     * create timeline with given params for rotating node.
     *
     * @param node     node for rotating
     * @param duration rotation period
     * @return Timeline
     */
    Animation getRotate(Node node, Duration duration);

    /**
     * create list of transitions for list of nodes for moving.
     *
     * @param nodeList list of <T>
     * @param duration duration of moving
     * @param width    width of parent
     * @param <T>      object extends from Node
     * @return List of Node
     */
    <T extends Node> List<Animation> getMoving(List<T> nodeList,
                                               Duration duration,
                                               int width);
}
