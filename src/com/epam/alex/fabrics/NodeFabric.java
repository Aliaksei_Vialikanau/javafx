package com.epam.alex.fabrics;

import javafx.scene.paint.Color;
import javafx.scene.shape.Box;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;

import java.awt.Point;
import java.util.List;

/**
 * Created by Алекс on 19.11.2016.
 */
public interface NodeFabric {
    /**
     * create cube with parameters as in arguments.
     *
     * @param size  size of cube
     * @param point  position of cube
     * @param color color of cube
     * @return Box object
     */
    Box getCube(int size, Point point, Color color);

    /**
     * create list of pairs dots with parameters as in arguments.
     * another dot in pair has offset by x axis equaled maxWidth param
     * relative to first dot
     *
     * @param maxWidth  max value of x axis
     * @param maxHeight max value of y axis
     * @param radius radius of circle
     * @return List of Circle
     */
    List<Circle> getCircles(int maxWidth, int maxHeight, int radius);

    /**
     * create text with parameters as in arguments.
     *
     * @param value text value
     * @param size  size of Text
     * @param color color of Text
     * @return Text
     */
    Text getText(String value, int size, Color color);
}
