package com.epam.alex.fabrics;

import javafx.animation.Animation;
import javafx.animation.Interpolator;
import javafx.animation.Timeline;
import javafx.animation.TranslateTransition;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.scene.Node;
import javafx.scene.transform.Rotate;
import javafx.util.Duration;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Алекс on 19.11.2016.
 */
public class AnimationFabricImpl implements AnimationFabric {

    /**
     * max angle of rotation.
     */
    private static final int ANGLE_MAX = 360;
    /**
     * step of angle.
     */
    private static final int ANGLE_STEP = 90;

    @Override
    public final Animation getMoving(final Node node, final Duration duration,
                                     final int startPos, final int endPos) {
        TranslateTransition transition =
                new TranslateTransition(duration, node);
        transition.setFromX(startPos);
        transition.setToX(endPos);
        transition.setInterpolator(Interpolator.LINEAR);
        transition.setCycleCount(1);
        return transition;
    }

    @Override
    public final Animation getRotate(final Node node,
                                     final Duration baseDuration) {
        Timeline timeline = new Timeline();
        Rotate rxBox = new Rotate(0, 0, 0, 0, Rotate.X_AXIS);
        Rotate rzBox = new Rotate(0, 0, 0, 0, Rotate.Z_AXIS);
        node.getTransforms().addAll(rxBox, rzBox);


        Duration duration = Duration.ZERO;
        for (int i = 0; i < ANGLE_MAX; i = i + ANGLE_STEP) {
            timeline.getKeyFrames().addAll(
                    new KeyFrame(duration,
                            new KeyValue(rxBox.angleProperty(), i),
                            new KeyValue(rzBox.angleProperty(), ANGLE_MAX)
                    ));
            duration = duration.add(baseDuration);
            timeline.getKeyFrames().addAll(
                    new KeyFrame(duration,
                            new KeyValue(rxBox.angleProperty(), i + ANGLE_STEP),
                            new KeyValue(rzBox.angleProperty(), 0)
                    )
            );
        }
        timeline.setCycleCount(Animation.INDEFINITE);
        return timeline;
    }


    @Override
    public final <T extends Node>
    List<Animation> getMoving(final List<T> nodes,
                              final Duration duration,
                              final int width) {
        List<Animation> transitions = new ArrayList<Animation>();
        for (T node : nodes) {
            Animation transition = getMoving(node, duration, -width, 0);
            transition.setCycleCount(Animation.INDEFINITE);
            transitions.add(transition);
        }
        return transitions;
    }


}
