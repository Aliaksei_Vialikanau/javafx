package com.epam.alex;

import com.epam.alex.fabrics.AnimationFabric;
import com.epam.alex.fabrics.AnimationFabricImpl;
import com.epam.alex.fabrics.NodeFabric;
import com.epam.alex.fabrics.NodeFabricImpl;
import javafx.animation.Animation;
import javafx.animation.Interpolator;
import javafx.animation.ParallelTransition;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.scene.shape.Box;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Shape;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.awt.Point;
import java.util.List;


/**
 * Created by Alex on 17.11.2016.
 */
public class JavaFX extends Application {

    /**
     * form width.
     */
    private static final int WIDTH = 320;

    /**
     * form height.
     */
    private static final int HEIGHT = 240;

    /**
     * basic duration of animation.
     */
    private static final int DURATION = 5000;
    /**
     * count of backgrounds.
     */
    private static final int BACKGOUND_COUNT = 5;

    /**
     * size of cube.
     */
    private static final int CUBE_SIZE = 50;

    /**
     * position of cube.
     */
    private static final Point CUBE_POS =
            new Point(WIDTH + CUBE_SIZE, HEIGHT / 3);
    /**
     * position of ending into.
     */
    private static final int DURATION_INTRO = 8000;

    /**
     * postionf of starting outro.
     */
    private static final int DURATION_OUTRO = 42000;

    /**
     * background layer of cube.
     */
    private static final int CUBE_DEPTH = 1;

    /**
     * marker of outro.
     */
    private static final String OUTRO_MARKER = "OUTRO_MARKER";

    /**
     * font size.
     */
    private static final int FONT_SIZE = 46;

    /**
     * text value.
     */
    private static final String TEXT_VALUE = " JavaFX demo ";

    /**
     * path to audio resources.
     */
    private static final String AXELF_RES = "/audio/axelf.mp3";

    /**
     * max color brighness.
     */
    private static final double MAX_COLOR_VALUE = 255.0;

    /**
     * multipler for cicrcles parent width.
     */
    private static final int STAR_MULTIPLER = 3;

    /**
     * radius of stars
     */
    private static final int STAR_RADIUS = 1;

    /**
     * main class.
     *
     * @param primaryStage primary stage
     * @throws Exception throw exception if something occurs
     */
    public final void start(final Stage primaryStage) throws Exception {

        Group root = new Group();
        Scene scene = new Scene(root,
                WIDTH, HEIGHT, Color.BLACK);
        primaryStage.setResizable(false);
        primaryStage.setScene(scene);

        NodeFabric nodeFabric = new NodeFabricImpl();
        Box cube = nodeFabric.getCube(CUBE_SIZE,
                CUBE_POS, Color.LIGHTSLATEGREY);
        Text text = getText(nodeFabric);

        AnimationFabric animationFabric = new AnimationFabricImpl();
        Animation starsAnimation = getStarsAnimation(root, nodeFabric,
                cube, text, animationFabric);

        Animation rotateTransition = animationFabric.getRotate(cube,
                Duration.millis(DURATION));

        Animation cubeIncoming = getInAnimation(cube, animationFabric);
        cubeIncoming.setOnFinished(event -> rotateTransition.play());

        Animation cubeOutgoing = getOutAnimation(cube, animationFabric);
        cubeOutgoing.setOnFinished(event -> primaryStage.close());

        Animation movingTextTimeline = getTextAnimation(text, animationFabric);

        starsAnimation.play();

        MediaPlayer mediaPlayer = getMediaPlayer(cubeIncoming, cubeOutgoing,
                movingTextTimeline);
        mediaPlayer.play();

        primaryStage.show();
    }

    /**
     * create mediaplayer with predifinded music.
     *
     * @param cubeIncoming       incoming cube animation
     * @param cubeOutgoing       outgoing cube animation.
     * @param movingTextTimeline moving text animation.
     * @return MediaPlayer.
     */
    private MediaPlayer getMediaPlayer(final Animation cubeIncoming,
                                       final Animation cubeOutgoing,
                                       final Animation movingTextTimeline) {
        Media media = new Media(getClass().getResource(AXELF_RES).
                toExternalForm());
        media.getMarkers().put(OUTRO_MARKER, Duration.millis(DURATION_OUTRO));

        MediaPlayer mediaPlayer = new MediaPlayer(media);
        mediaPlayer.setOnPlaying(() -> {
            movingTextTimeline.play();
            cubeIncoming.play();
        });
        mediaPlayer.setOnMarker(event -> {
            if (event.getMarker().getKey().equals(OUTRO_MARKER)) {
                cubeOutgoing.play();
            }
        });
        return mediaPlayer;
    }

    /**
     * create animation of outgoing node.
     *
     * @param node            node for animation
     * @param animationFabric fabric for creating animation
     * @return Animation
     */
    private Animation getOutAnimation(final Node node,
                                      final AnimationFabric animationFabric) {
        int startPos = WIDTH / 2;
        int endPos = (int) -node.getBoundsInParent().getWidth();
        Animation cubeOutgoing = animationFabric.getMoving(node,
                Duration.millis(DURATION), startPos, endPos);

        cubeOutgoing.setCycleCount(1);
        return cubeOutgoing;
    }

    /**
     * create animation of incoming node.
     *
     * @param node            node for animation
     * @param animationFabric fabric for creating animation
     * @return Animation
     */
    private Animation getInAnimation(final Node node,
                                     final AnimationFabric animationFabric) {
        int startPos = (int) (WIDTH + node.getBoundsInParent().getWidth());
        int endPos = WIDTH / 2;
        Animation cubeIncoming = animationFabric.getMoving(node,
                Duration.millis(DURATION_INTRO), startPos, endPos);
        cubeIncoming.setCycleCount(1);
        return cubeIncoming;
    }

    /**
     * create moving transition with circles.
     *
     * @param root            root to add circles
     * @param nodeFabric      fabric for creating circles
     * @param cube            cube
     * @param text            text
     * @param animationFabric fabric for creating animations
     * @return ParallelTransition
     */
    private Animation getStarsAnimation(final Group root,
                                        final NodeFabric nodeFabric,
                                        final Box cube,
                                        final Text text,
                                        final AnimationFabric animationFabric) {
        ParallelTransition parallelTransition = new ParallelTransition();
        for (int i = BACKGOUND_COUNT - 1; i >= 0; i--) {
            List<Circle> nodes = nodeFabric.getCircles(WIDTH * STAR_MULTIPLER,
                    HEIGHT, STAR_RADIUS);
            Duration duration = Duration.millis(DURATION * STAR_MULTIPLER
                    * (i + 1));
            Color color = getColor(i, BACKGOUND_COUNT - 1);
            setColor(nodes, color);
            root.getChildren().addAll(nodes);
            if (i == CUBE_DEPTH) {
                root.getChildren().addAll(cube, text);
            }
            parallelTransition.getChildren().addAll(
                    animationFabric.getMoving(nodes, duration,
                            WIDTH * STAR_MULTIPLER));

        }
        parallelTransition.setInterpolator(Interpolator.LINEAR);
        return parallelTransition;
    }


    /**
     * create text.
     *
     * @param nodeFabric NodeFabric
     * @return Text
     */
    private Text getText(final NodeFabric nodeFabric) {
        Text text = nodeFabric.getText(TEXT_VALUE, FONT_SIZE,
                Color.LIGHTYELLOW);
        text.setTranslateY(HEIGHT - text.getFont().getSize());
        text.setTranslateX(WIDTH);
        return text;
    }

    /**
     * create text animation.
     *
     * @param text            text to animate
     * @param animationFabric AnimationFabric
     * @return Animation
     */
    private Animation getTextAnimation(final Text text,
                                       final AnimationFabric animationFabric) {
        int startPos = (int) (WIDTH + text.getBoundsInParent().getWidth());
        int endPos = (int) -text.getBoundsInParent().getWidth();
        Animation movingTextTimeline = animationFabric.getMoving(text,
                Duration.millis(DURATION * STAR_MULTIPLER), startPos, endPos);
        movingTextTimeline.setCycleCount(Animation.INDEFINITE);
        return movingTextTimeline;
    }

    /**
     * set color to shapes.
     *
     * @param shapes list of Shape
     * @param color  setted color
     * @param <T>    object extended from Shape
     */
    private <T extends Shape> void setColor(final List<T> shapes,
                                            final Color color) {
        for (T shape : shapes) {
            shape.setFill(color);
        }
    }


    /**
     * get color aqua with an RGB value of #00FFFF with brigthness
     * choosed by layer index.
     * first layer has maximum brigthness, last - minimal.
     *
     * @param index index of current layer
     * @param count count of layers
     * @return Color
     */
    private Color getColor(final int index, final int count) {
        int color = (int) (MAX_COLOR_VALUE - (MAX_COLOR_VALUE / count * index));
        if (color < 0) {
            color = 0;
        }
        return Color.rgb(0, color, color);
    }


}
