package com.epam.alex.ant;

import org.apache.tools.ant.*;

import java.io.File;
import java.util.*;

/**
 * Created by Алекс on 19.11.2016.
 */
public class BuildValidator extends Task {


    /**
     * name of build file.
     */
    private String filename;

    private Set<Checker> checkers;

    /**
     * constructs new BuildValidator with default values.
     */
    public BuildValidator() {
        filename = Constants.BUILD_XML;
        checkers = new HashSet<>();
    }


    /**
     * set and output to console buildfile value.
     *
     * @param value new build filename
     */
    public final void setBuildfile(final String value) {
        filename = value;
        System.out.println(Constants.BUILD_FILE_IS + filename);
    }

    /**
     * set checkdepends value.
     *
     * @param value new flag
     */
    public final void setDepends(final boolean value) {
        if (value) {
            checkers.add(Checkers.DEPENDS);
        } else {
            checkers.remove(Checkers.DEPENDS);
        }
    }

    /**
     * set checkdefault value.
     *
     * @param value new flag
     */
    public final void setDefault(final boolean value) {
        if (value) {
            checkers.add(Checkers.DEFAULT);
        } else {
            checkers.remove(Checkers.DEFAULT);
        }
    }

    /**
     * set checkNames value.
     *
     * @param value new flag
     */
    public final void setNames(final boolean value) {
        if (value) {
            checkers.add(Checkers.NAMES);
        } else {
            checkers.remove(Checkers.NAMES);
        }
    }

    @Override
    public final void execute() throws BuildException {
        Project project = new Project();
        project.setUserProperty("ant.file", filename);
        project.init();
        ProjectHelper helper = ProjectHelper.getProjectHelper();
        project.addReference("ant.projectHelper", helper);
        File file = new File(filename);
        if (file.exists() && file.isFile()) {
            helper.parse(project, file);
            boolean isValid = true;
            for (Checker checker : checkers) {
                isValid = checker.isValid(project) && isValid;
            }
            if (!isValid) {
                throw new BuildException(Constants.ERROR_FILE_NOT_VALID);
            }
        } else {
            throw new BuildException(Constants.ERROR_FILE_NOT_FOUND);
        }
    }
}
