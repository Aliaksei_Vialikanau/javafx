package com.epam.alex.ant;

/**
 * Created by pups on 21.11.2016.
 */
public final class Validate {
    /**
     * start method.
     *
     * @param args command line values
     */
    public static void main(final String... args) {
        BuildValidator buildValidator = new BuildValidator();
        buildValidator.setDefault(true);
        buildValidator.setNames(true);
        buildValidator.setDepends(true);
        buildValidator.execute();
    }

    /**
     * default constructor.
     */
    private Validate() {

    }
}
