package com.epam.alex.ant;

import org.apache.tools.ant.Project;
import org.apache.tools.ant.Target;

import java.util.Enumeration;

/**
 * Created by Алекс on 24.11.2016.
 */
public enum Checkers implements Checker {
    /**
     * Checker for validate default target.
     */
    DEFAULT {
        @Override
        public boolean isValid(final Project project) {
            String defaultTarget = project.getDefaultTarget();
            if (isEmpty(defaultTarget)) {
                System.out.println(Constants.ERROR_MESSAGE_DEFAULT);
                return false;
            }
            return true;
        }
    },
    /**
     * Checker for validate names of targets. {@link Constants#VALID_NAME}.
     */
    NAMES {
        @Override
        public boolean isValid(final Project project) {
            Enumeration<Target> targets = project.getTargets().elements();
            while (targets.hasMoreElements()) {
                Target target = targets.nextElement();
                String targetName = target.getName();
                if (!isEmpty(targetName)) {
                    if (!targetName.matches(Constants.VALID_NAME)) {
                        System.out.println(String.format(Constants.ERROR_MESSAGE_NAMES, targetName));
                        return true;
                    }
                }
            }
            return true;
        }
    },
    /**
     * Checker for validate is depends are only in main target.
     */
    DEPENDS {
        @Override
        public boolean isValid(final Project project) {
            Enumeration<Target> targets = project.getTargets().elements();
            while (targets.hasMoreElements()) {
                Target target = targets.nextElement();
                String defaultTarget = project.getDefaultTarget();
                if (isEmpty(defaultTarget)) {
                    defaultTarget = Constants.MAIN;
                }
                String targetName = target.getName();
                if (!isEmpty(targetName)) {
                    if (!targetName.equals(defaultTarget) && target.getDependencies().hasMoreElements()) {
                        System.out.println(String.format(Constants.ERROR_MESSAGE_DEPENDS, targetName));
                        return false;
                    }
                }
            }
            return true;
        }


    };

    boolean isEmpty(final String value) {
        return value == null || value.trim().isEmpty();
    }

}
