package com.epam.alex.ant;

/**
 * Created by Алекс on 19.11.2016.
 */
public final class Constants {
    /**
     * default build filename.
     */
    public static final String BUILD_XML = "build.xml";
    /**
     * sax validation url.
     */
    public static final String SAX_VALIDATION =
            "http://xml.org/sax/features/validation";
    /**
     * apache validation url.
     */
    public static final String APACHE_VALIDATION =
            "http://apache.org/xml/features/validation/schema";
    /**
     * regexp for checking valid target's name.
     */
    public static final String VALID_NAME = "[a-zA-Z\\-]+";
    /**
     * name of valid target's name for 'DEPENDS' attribute.
     */
    public static final String MAIN = "main";
    /**
     * error message for non valid name.
     */
    public static final String ERROR_MESSAGE_NAMES =
            "Target '%s' must contains only letters with '-'";
    /**
     * error message for non valid depends attribute.
     */
    public static final String ERROR_MESSAGE_DEPENDS =
            "Target '%s' with depends are used instead of 'main' point";
    /**
     * error message for empty default attribute.
     */
    public static final String ERROR_MESSAGE_DEFAULT =
            "Project has't default attribute.";

    /**
     * message for outpute build file name.
     */
    public static final String BUILD_FILE_IS = "Build file is ";
    /**
     * exception message for non valid build file.
     */
    public static final String ERROR_FILE_NOT_VALID = "Build file is not valid.";

    /**
     * exception message for not founded build file.
     */
    public static final String ERROR_FILE_NOT_FOUND = "File not found.";

    /**
     * default constructor.
     */
    private Constants() {
    }

}
