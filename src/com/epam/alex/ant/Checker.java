package com.epam.alex.ant;

import org.apache.tools.ant.Project;

/**
 * Created by Алекс on 24.11.2016.
 */
public interface Checker {
    /**
     * validate project.
     * @param project Project to validate
     * @return true if project valid
     */
    boolean isValid(final Project project);
}
